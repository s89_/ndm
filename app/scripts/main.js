
// jQuery for smooth page scrolling

$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});


// function to rotate client images
function clientRotator(){

  var section = document.getElementById("clients");
  var clientImg = document.createElement("img");
  clientImg.setAttribute("id", "rotator");
  section.appendChild(clientImg);
  clientImg.src = "assets/exxon-mobil.png";
  var imgArray = ["assets/shell.png", "assets/bp.png", "assets/talisman.png", "assets/cnr-international.png", "assets/exxon-mobil.png"];

  var delaySeconds = 3;
  var iteration = 0;

  setInterval(function(){

    section.removeChild(clientImg);
    clientImg = document.createElement("img");
    clientImg.setAttribute("id", "rotator");
    section.appendChild(clientImg);
    clientImg.src = imgArray[iteration];
    clientImg.setAttribute("alt", "Oil & Gas - Gas Turbine Overhauls");

    if (iteration < imgArray.length-1){
      iteration += 1;
    }
    else {
      iteration = 0;
    }
  }, delaySeconds * 1000)

}


// function to toggle fontawesome hamburger menu icon

function menuToggle(){
  var menu = document.getElementById("menu");
  menu.addEventListener("click", function changeClass(){
    if ( document.getElementById("menu").classList.contains('fa-bars') ){
      document.getElementById("menu").classList.remove('fa-bars');
      document.getElementById("menu").classList.add('fa-times');
      document.getElementById("contact-li").setAttribute("style", "display: inline ");
      document.getElementById("about-li").setAttribute("style", "display: inline ");
    }
    else {
      document.getElementById("menu").classList.remove('fa-times');
      document.getElementById("menu").classList.add('fa-bars');
      document.getElementById("contact-li").setAttribute("style", "display: none ");
      document.getElementById("about-li").setAttribute("style", "display: none ");
    }

  })
}

// Event listener for image rotator
window.addEventListener("load", clientRotator);

// Event listener for responsive menu icon toggle
window.addEventListener("load", menuToggle);
